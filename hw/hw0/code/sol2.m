data_dir = '../data/hw0_data/';
output_dir = '../data/output/';

file_suffix_list= {'1.png', '2.png', '3.jpg', '4.jpg'};
for id_file_suffix=1:4
    
    out_img = center_plus_border([data_dir, '/center', file_suffix_list{id_file_suffix}], ...
                                 [data_dir, '/border', file_suffix_list{id_file_suffix}]);
    imwrite(out_img, [output_dir, '/out', num2str(id_file_suffix) '.png']);
end                            
