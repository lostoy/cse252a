function [out_img] = center_plus_border(center_img_path, border_img_path)
center_img = imread(center_img_path);
border_img = imread(border_img_path);

[h_border, w_border, c_border] = size(border_img);
[h_center, w_center, c_center] = size(center_img);

if c_border ~= c_center
    error('channel doesn''t match!')
end

if h_center > h_border || w_center > w_border
    error('size doesn''t match!')
end

out_img = border_img;
h_margin = floor((h_border - h_center) / 2);
w_margin = floor((w_border - w_center) / 2);
out_img(h_margin+1:h_margin+h_center, w_margin+1:w_margin+w_center, :) = center_img;

