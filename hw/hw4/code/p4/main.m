clear 
close all
clc
addpath ../p3
addpath ..

% Compute fisherfaces
[trainset,trainlabels] = loadSubset(0,'../../data/yaleBfaces/');
trainset = trainset';
[W,mu] = fisherTrain(trainset,trainlabels,10);

% Show fisherfaces
figure;
fisherFaces = W(:,1:9);
imagesc(drawFaces(fisherFaces',5)); daspect([1 1 1]); colormap gray
set(gca,'xtick',[],'ytick',[])

% Evaluate fisherfaces
acc = zeros(9,4);
for s = 1:4
    [testset,testlabels] = loadSubset(s,'../../data/yaleBfaces/');
    testset = testset';
    for k = 1:9
        testpredictions=eigenTest(trainset,trainlabels,testset,W,mu,k);
        acc(k,s) = mean(testpredictions~=testlabels);
    end
end
figure; plot(acc*100,'linewidth',2);
legend({'Subset 1','Subset 2','Subset 3','Subset 4'})
set(gca,'DefaultTextFontSize',18);
xlabel('k'); ylabel('Error rate [%]'); grid on