function[W,mu]=fisherTrain(trainset,trainlabels,c)
nCls = length(unique(trainlabels));
nSmp = size(trainset,2);

% Dimensionality reduction using PCA
[Wpca,mu] = eigenTrain(trainset,nSmp-c);
X = Wpca'*bsxfun(@minus,trainset,mu);

% Sample means in lower dimensional space
mu_all = mean(trainset,2);
mu_y = zeros(size(trainset,1),nCls);
n_y = zeros(1,nCls);
for y = 1:nCls
    n_y(y) = sum(trainlabels==y);
    mu_y(:,y) = mean(trainset(:,trainlabels==y),2);
end

% Within and Between-Class Covariances
Sb = zeros(size(trainset,1));
Sw = zeros(size(trainset,1));
for y = 1:nCls
    idx_y = trainlabels==y;
    Sb = Sb + n_y(y) * (mu_y(:,y)-mu_all) *(mu_y(:,y)-mu_all)';
    Sw = Sw + bsxfun(@minus,trainset(:,idx_y),mu_y(:,y))*bsxfun(@minus,trainset(:,idx_y),mu_y(:,y))';
end

% Compute FisherFaces
[Wfld,~] = eigs(Wpca'*Sb*Wpca,Wpca'*Sw*Wpca,c-1);
W = Wpca*Wfld;