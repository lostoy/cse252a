function testlabels=eigenTest(trainset,trainlabels,testset,W,mu,k)
trainPCAcomp = W(:,1:k)'*bsxfun(@minus,trainset,mu);
testPCAcomp = W(:,1:k)'*bsxfun(@minus,testset,mu);

idx = knnsearch(trainPCAcomp',testPCAcomp');
testlabels = trainlabels(idx);