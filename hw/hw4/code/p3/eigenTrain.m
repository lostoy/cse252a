function [W,mu]=eigenTrain(trainset,k)
mu = mean(trainset,2);
trainset = bsxfun(@minus,trainset,mu);
[W,~,~] = svds(trainset,k);