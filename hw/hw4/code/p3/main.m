clear 
close all
clc
addpath ../

% Compute eigenfaces
[trainset,trainlabels] = loadSubset(0,'../../data/yaleBfaces/');
trainset = trainset';
[W,mu] = eigenTrain(trainset,30);

% Show eigenfaces
figure; imshow(reshape(mu,[50 50]))
figure; eigenFaces = W(:,1:20);
imagesc(drawFaces(eigenFaces',10)); daspect([1 1 1]); colormap gray
set(gca,'xtick',[],'ytick',[])

% Show reconstructions
figure
idx = [];
for y = 1:10, idx = [idx find(trainlabels==y,1)]; end
for k = 1:10
    PCAproj = W(:,1:k)'*bsxfun(@minus,trainset(:,idx),mu);
    PCArec = bsxfun(@plus,W(:,1:k)*PCAproj,mu);
    subplot(2,5,k); imshow(drawFaces(PCArec',2));
    title(['k = ' num2str(k)])
end

% Evaluate eigenfaces
acc = zeros(20,4);
for s = 1:4
    [testset,testlabels] = loadSubset(s,'../../data/yaleBfaces/');
    testset = testset';
    for k = 1:20
        pred = eigenTest(trainset,trainlabels,testset,W,mu,k);
        acc(k,s) = mean(pred~=testlabels);
    end
end
figure; plot(acc*100,'linewidth',2);
set(gca,'DefaultTextFontSize',18);
legend({'Subset 1','Subset 2','Subset 3','Subset 4'})
xlabel('k'); ylabel('Error rate [%]'); grid on

acc = zeros(20,4);
for s = 1:4
    [testset,testlabels] = loadSubset(s,'../../data/yaleBfaces/');
    testset = testset';
    for k = 1:20
        pred=eigenTest(trainset,trainlabels,testset,W(:,5:end),mu,k);
        acc(k,s) = mean(pred~=testlabels);
    end
end
figure; plot(acc*100,'linewidth',2);
legend({'Subset 1','Subset 2','Subset 3','Subset 4'})
set(gca,'DefaultTextFontSize',18);
xlabel('k'); ylabel('Error rate [%]'); grid on

