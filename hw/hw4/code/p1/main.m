%%
data_dir = '../../data/yaleBfaces/';
output_dir = '../../data/output/p1/';
%%
[trainset, trainlabels] = loadSubset(0, data_dir);
predict_y_list = cell(4,3);
acc_list = zeros(4,3);
k_nn_list = [1,3,5];
%%
for test_set_id = 1:4;

    [testset, testlabels] = loadSubset(test_set_id, data_dir);
    dist = euclidean_dist(testset, trainset);

    for k_nn_id = 1:3
        k_nn = k_nn_list(k_nn_id);
        
        [predict_y] = knn_from_dist(dist, trainlabels, k_nn, 10);
        acc = sum(predict_y == testlabels) / length(testlabels);
        
        fprintf('accuracy for test set %d with k_nn %d, is :%f\n', test_set_id, k_nn, acc);
        predict_y_list{test_set_id, k_nn_id} = predict_y;
        acc_list(test_set_id, k_nn_id) = acc;
        
        if k_nn_id == 1
            figure(1)
            t_rnd_ind = datasample(find(predict_y==testlabels), 1);
            subplot(1,2,1);
            imshow(drawFaces(testset(t_rnd_ind,:),1))
            title('test img')
            [~, t_train_ind] = min(dist(t_rnd_ind, :));
            subplot(1,2,2);
            imshow(drawFaces(trainset(t_train_ind, :), 1));
            title('1-nn match train img')
            saveas(gcf, sprintf('%s/testset%d_t.eps', output_dir, test_set_id), 'epsc');
            
            figure(2)
            f_rnd_ind = datasample(find(predict_y~=testlabels), 1);
            subplot(1,2,1);
            imshow(drawFaces(testset(f_rnd_ind,:),1))
            title('test img')
            [~, f_train_ind] = min(dist(f_rnd_ind, :));
            subplot(1,2,2);
            imshow(drawFaces(trainset(f_train_ind, :), 1));
            title('1-nn match train img')
            saveas(gcf, sprintf('%s/testset%d_f.eps', output_dir, test_set_id), 'epsc');
        end
        
        
    end
    fprintf('\n');
end

    

