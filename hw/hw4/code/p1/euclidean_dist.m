function [dist] = euclidean_dist(test, train)
% dataset as N*D
diff = bsxfun(@minus, permute(test, [1,3,2]), permute(train, [3,1,2]));
dist = sqrt(sum(diff.^2, 3));
end