function [predict_y] = knn_from_dist(dist, train_labels, k_nn, n_label)
N = size(dist, 1);
vote = zeros(N, n_label);
[~, ind] = sort(dist, 2);
for vote_id=1:k_nn
    tmp_ind = sub2ind(size(vote), 1:N, train_labels(ind(:, vote_id))');
    vote(tmp_ind) = vote(tmp_ind) + 1;
end
[~, predict_y] = max(vote, [], 2);
end

