\documentclass{assignment}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{subcaption}
\lstset{
numbers=left
}

\coursetitle{Computer Vision I}
\courselabel{CSE 252A}
\exercisesheet{Homework 4}{}
\student{Li, Yingwei and Morgado, Pedro}
\university{University of California, San Diego}
\semester{Fall 2015}
\date{\today}


\newcommand{\p}{\mathbf{p}}
\newcommand{\x}{\mathbf{x}}
\newcommand{\bP}{\mathbf{P}}

\begin{document}
\begin{problemlist}
\pbitem {\it Naive Recognition}
\paragraph{Part 1} 
With 1-NN the test accuracy for the 4 test sets are: 0.941667, 0.516667, 0.192857 and 0.152632 respectively.

\paragraph{Part 2}
We can see that the test accuracy decreases with the four test sets. This makes sense, since the test sets becomes more and more different than the training set, in terms of lighting condition and pose. This makes the classification harder hence the test accuracy decreases.

\paragraph{Part 3}
Figure~\ref{fig:p1:correct_cases} shows two correctly classified test cases and their corresponding 1-nn matches in the training set. We can see that the test images are quite similar to the training image in pose and lighting condition, thus it's reasonable to have good prediction.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\linewidth]{../data/output/p1/testset1_t.eps}
	\includegraphics[width=0.5\linewidth]{../data/output/p1/testset2_t.eps}
	\caption{Correct classified faces.}
	\label{fig:p1:correct_cases}
\end{figure}

\paragraph{Part 4}
Figure~\ref{fig:p4:wrong_cases} shows two mis-classified test cases and their corresponding 1-nn matches in the training set. We can see that the test images are lightened differently than the training images, thus it's reasonable to hard to find a 1-nn match in the training set.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\linewidth]{../data/output/p1/testset3_f.eps}
	\includegraphics[width=0.5\linewidth]{../data/output/p1/testset4_f.eps}
	\caption{Mis-classified faces.}
	\label{fig:p1:wrong_cases}
\end{figure}

\pbitem {\it k-NN Recognition}
\paragraph{Part 1} 
Table~\ref{tab:p2:acc} lists the test accuracy for the four test sets with $k$=1, 3 and 5. 
\paragraph{Part 2}
Compared to 1-NN, the performance increases for testset 1 as we increase $k$ to 3 and 5, but decreases for the other three test sets. For test set 1, the matching is more robust to noise as k increases. But for the other three sets, the images are of different lighting conditions, so by having larger $k$, the classification result has a even larger variance. So the performance decreases.

\begin{table}[htdp]
\caption{Test accuracy for k-nn}
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
	&	testset 1	&	testset 2	&	testset 3	&	testset 4\\
\hline
k=1	&	0.941667	&	0.516667	&	0.192857	&	0.152632\\
k=3	&	0.941667	&	0.475000	&	0.178571	&	0.121053\\
k=5	&	0.950000	&	0.466667	&	0.171429	&	0.110526\\
\hline
\end{tabular}
\end{center}
\label{tab:p2:acc}
\end{table}%

\pbitem {\it Recognition Using Eigenfaces}
\paragraph{Part 1 - Computing eigenfaces:} The function \textsf{eigenTrain()} for eigenface computation was implemented and can be seen in attach. 

\paragraph{Part 2 - Visualizing eigenfaces:} The results of eigenface computation, using subset 0 of Yale Face Database as training set, is shown in Figures \ref{fig:p3:mean} and \ref{fig:p3:eigenface}.

\begin{figure}[t!]
	\centering
	\includegraphics[height=3cm,trim={470 250 470 120},clip=true]{images/p3_mean.png}
	\caption{Mean face.}
	\label{fig:p3:mean}
\end{figure}
\begin{figure}[t!]
	\centering
	\includegraphics[width=\textwidth,trim={150 60 100 50},clip=true]{images/p3_eigenface.png}
	\caption{Top 20 eigenfaces.}
	\label{fig:p3:eigenface}
\end{figure}

\paragraph{Part 3 - Explaining eigenfaces:} Eigenfaces are the eigenvectors of the covariance matrix associated with the mean-deviation form of the image data. Hence, these eigenvectors will form a linear subspace where the image data can be alternatively represented. If we use only the $k$ eigenvectors associated with the largest eigenvalues, we are representing the image on the $k$-dimensional linear subspace over which the data have maximum variance, i.e. we are discarding the directions along which the images of the faces appear to have no differences between them. So, using PCA allows us to reduce the dimensionality of the image data (which in this problem lies in a 2500-dimensional subspace), while preserving most of the information. 

\paragraph{Part 4 - Reconstruction:}
The face reconstruction of each individual using only the top $k$ eigenfaces is shown in Fig.~\ref{fig:p3:PCArec}. Notice that, when using only 1 eigenface, many individuals cannot be discriminated between each other, but as more eigenfaces are included in the reconstruction process, the finer details that distinguish the different people start to appear in the reconstructed images.
\begin{figure}[t!]
	\centering
	\includegraphics[width=\textwidth,trim={90 60 70 30},clip=true]{images/p3_rec.png}
	\caption{Reconstruction of images of 10 different individuals using only $k$ eigenfaces. Results are shown with $k$ from 1 to 10.}
	\label{fig:p3:PCArec}
\end{figure}  

\paragraph{Part 5 - Classifying with k eigenfaces:} The function \textsf{eigenTest()}, which classifies test images using a 1-NN on the $k$-dimensional eigenspace, is shown in attach. The error rates for 4 different test subsets of the Yale Face Database is shown in Fig.~\ref{fig:p3:acc_topk}. As can be seen, as more eigenfaces are used for classification, the results tend to improve on all subsets, especially on the easier ones, i.e. subset 1 and 2. As can be seen in Fig.~\ref{fig:p3:sample}, subsets 3 and 4 are harder to classify because illumination conditions are significantly different from the training set, including extreme samples where part of the face are shadowed. The very poor results on these two subsets should therefore be expected, because shadowed images do not lie on a linear manifold of the data, which makes the PCA linear compression very lossy.
\begin{figure}[t!]
	\centering
	\includegraphics[width=0.6\textwidth]{images/p3_acc_topk.png}
	\caption{Error rates on 4 subsets of the YFDB of increasing difficulty, using the top $k$ eigenfaces.}
	\label{fig:p3:acc_topk}
\end{figure} 

\begin{figure}[t!]
	\centering
	\begin{subfigure}[b]{0.18\textwidth}
		\includegraphics[width=\textwidth]{images/person1_0.png}
		\caption{Subset 0}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.18\textwidth}
		\includegraphics[width=\textwidth]{images/person1_1.png}
		\caption{Subset 1}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.18\textwidth}
		\includegraphics[width=\textwidth]{images/person1_2.png}
		\caption{Subset 2}
	\end{subfigure}
	\begin{subfigure}[b]{0.18\textwidth}
		\includegraphics[width=\textwidth]{images/person1_3.png}
		\caption{Subset 3}
	\end{subfigure}
	\begin{subfigure}[b]{0.18\textwidth}
		\includegraphics[width=\textwidth]{images/person1_4.png}
		\caption{Subset 4}
	\end{subfigure}
	\caption{Sample images of person 1 in the 5 different subsets of Yale Faces Database.}
	\label{fig:p3:sample}
\end{figure}

\paragraph{Part 6 - Discarding top 4 eigenfaces:} The same evaluation procedure as in part 5 was carried, but now discarding the first 4 eigenfaces. The results are shown in Fig.~\ref{fig:p3:acc_topk-5}. Notice that there is a significant improvement in performance on all subsets, and for most values of $k$. This suggests that the first few eigenfaces do not contain very discriminant information. In fact, this should not be surprising after seeing the reconstructions using different values of $k$. As noted in part 4, the fine differences between the different individuals are not encoded in the first few eigenfaces, reason why we cannot distinguish some pairs of individual (e.g. the two on the 3rd row) from the reconstruction using only 3 or 4 eigenfaces.

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.6\textwidth]{images/p3_acc_topk-5.png}
	\caption{Error rates on 4 subsets of the YFDB of increasing difficulty, using $k$ eigenfaces, but discarding the first 4.}
	\label{fig:p3:acc_topk-5}
\end{figure}

\paragraph{Part 7 - Explaning influence of parameter:} The influence of the subset difficulty and the number of eigenfaces was explained in part 5.

\pbitem {\it Recognition Using Fisherfaces}
\paragraph{Part 1 - Computing fisherfaces:} The function \textsf{fisherTrain()} for fisherface computation was implemented and can be seen in attach. This includes reducing first the dimension of the image data using PCA, computing the within-class $S_W$ and between-class $S_B$ scatter matrices, and solving the generalized eigen decomposition problem for $S_W$ and $S_B$.

\paragraph{Part 2 - Visualizing fisherfaces:} The 9 fisherfaces computed from subset 0 of Yale Face Database are shown in Figure \ref{fig:p4:fisherface}. The mean face is shown in Fig.~\ref{fig:p3:mean}.

\begin{figure}[t!]
	\centering
	\includegraphics[width=\textwidth,trim={120 60 80 50},clip=true]{images/p4_fisherface.png}
	\caption{Fisherfaces.}
	\label{fig:p4:fisherface}
\end{figure}

\paragraph{Part 3 - Classifying with fisherfaces:} Similarly to Problem 3, we evaluated the fisherface representation on the 4 subsets of the Yale Faces Database. Results are shown in Fig.~\ref{fig:p4:acc_topk}. As can be seen, the performance improves on all subsets except for subset 4. In fact, when using all fisherfaces, we attain the best overall recognition results: 0\% error rate on subset 1, 3.33\% on subset 2 and 60.72\% on subset 3. As for subset 4, no algorithm implemented in this homework was able to do significantly better than random guess, attaining error rates around 10\%. The improvement over eigenfaces is due to the fact that the fisherfaces representation is looking for a linear subspace of the data that maximizes the between-class scatter and minimizes the within class scatter. From a classification standpoint, the within-class scatter contains no useful information and so it can be suppressed during dimensionality reduction. Eigenfaces, on the other hand, maximize the total scatter, i.e. the combination of both within and between-class scatters.  So if differences in illumination, facial expression, etc., which contribute to the within-class scatter, is significant, then the eigenfaces will explain intraclass variability which is useless for classification.

As with eigenface representations, classification using fisherfaces also benefit from increasing $k$, since more information is being used, and in this problem, the curse of dimensionality seems not be play a role (probably because the number of dimensions is still small enough). Also, the test subsets 1 to 4 show an increased level of difficulty, with the trained classifier performing fairly well on subsets 1 and 2, poorly on subset 3 and very poorly (not even better than random guess) on subset 4.
This happens by the same reason explained on the eigenfaces: the extreme lighting conditions in subsets 3 and 4 cause large regions of the faces to be in shadow. Although images of faces without shadows and assuming a Lambertian surface lie on a 3D linear subspace, reason why a linear compression can achieve very good results in subsets 1 and 2, the shadows observed in subsets 3 and 4 break this assumption. Hence, important information is lost during dimensionality reduction and performance is suboptimal. 


\begin{figure}[t!]
	\centering
	\includegraphics[width=0.6\textwidth]{images/p4_acc_topk.png}
	\caption{Error rates on 4 subsets of the YFDB of increasing difficulty, using the top $k$ eigenfaces.}
	\label{fig:p4:acc_topk}
\end{figure} 


\end{problemlist}
\lstinputlisting[language=MATLAB, caption=Compute euclidean distance]{../code/p1/euclidean_dist.m}
\lstinputlisting[language=MATLAB, caption=get match given distance]{../code/p1/knn_from_dist.m}
\lstinputlisting[language=MATLAB, caption=Entry point for p1 and p2]{../code/p1/main.m}
\lstinputlisting[language=MATLAB, caption=Eigenface computation.]{../code/p3/eigenTrain.m}
\lstinputlisting[language=MATLAB, caption=Recognition using eigenfaces.]{../code/p3/eigenTest.m}
\lstinputlisting[language=MATLAB, caption=Script for generating all images for Problem 3.]{../code/p3/main.m}
\lstinputlisting[language=MATLAB, caption=Fisherface computation.]{../code/p4/fisherTrain.m}
\lstinputlisting[language=MATLAB, caption=Script for generating all images for Problem 4.]{../code/p4/main.m}

\end{document}
