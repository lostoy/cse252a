function corsSSD = correspondanceMatchingLine( I1, I2, corners1, F, R, SSDth)
corsSSD = [];
for c = 1:size(corners1,1)
    if ~(corners1(c,1)>R && corners1(c,2)>R && corners1(c,1)<size(I2,2)-R+1 && corners1(c,2)<size(I2,1)-R+1)
        continue
    end
    wind1 = I1(corners1(c,2)-R:corners1(c,2)+R,corners1(c,1)-R:corners1(c,1)+R,:);
    
    pts = linePts(F'*[corners1(c,:)';1], [1 size(I2,2)], [1 size(I2,1)]);
    Pi = pts(1,:); Pf = pts(2,:);
    dist = norm(Pf-Pi);

    SSDscore = inf*ones(1,ceil(dist));
    for i = 1:ceil(dist)
        alpha = i/dist;
        P = round(alpha*Pf+(1-alpha)*Pi);
        if ~(P(1)>R && P(2)>R && P(1)<size(I2,2)-R+1 && P(2)<size(I2,1)-R+1)
            continue
        end
        wind2 = I2(P(2)-R:P(2)+R,P(1)-R:P(1)+R,:);
        SSDscore(i) = SSDmatch(wind1,wind2);
    end
    [minSSD,i] = min(SSDscore);
    if minSSD <= SSDth
        alpha = i/dist;
        corsSSD(end+1,:) = [corners1(c,:) round(alpha*Pf+(1-alpha)*Pi)];
    end
end

