close all
clear all
clc
addpath ../../hw2_data/

%%
% close all
% load dino2.mat
% prms.windowSize = 11;
% prms.smoothSTD = 2;
% prms.R = 20;
% prms.SSDth = 150;
% prms.outlierTH = 20;
% plotAllResults(dino01,dino02,proj_dino01,proj_dino02,cor1,cor2,prms)

%%
% clear all
% close all
% load warrior2.mat
% prms.windowSize = 11;
% prms.smoothSTD = 2;
% prms.R = 20;
% prms.SSDth = 350;
% prms.outlierTH = 20;
% plotAllResults(warrior01,warrior02,proj_warrior01,proj_warrior02,cor1,cor2,prms)

%%
clear all
close all
load matrix2.mat
prms.windowSize = 11;
prms.smoothSTD = 2;
prms.R = 40;
prms.SSDth = 1000;
prms.outlierTH = 20;
plotAllResults(matrix01,matrix02,proj_matrix01,proj_matrix02,cor1,cor2,prms)
