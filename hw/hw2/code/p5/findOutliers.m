function [inlier, outlier] = findOutliers(points3D, P2, outlierTH, corsSSD)
outlier = false(size(points3D,1),1);
for i = 1:size(points3D,1)
    P = [points3D(i,:)';1];
    p2_proj = P2*P;
    p2_proj = p2_proj(1:2)/p2_proj(3);
    p2 = corsSSD(i,3:4)';
    
    if norm(p2-p2_proj)>outlierTH
        outlier(i) = true;
    end
end
inlier = ~outlier;