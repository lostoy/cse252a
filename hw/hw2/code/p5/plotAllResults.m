function plotAllResults(I1,I2,P1,P2,cor1,cor2,prms)
nCorners = 20;
smoothSTD = prms.smoothSTD;
windowSize = prms.windowSize;

corners1 = CornerDetect(I1, 50, smoothSTD, windowSize);
figure; subplot(121); imshow(I1); hold on;
plot(corners1(1:nCorners,1),corners1(1:nCorners,2),'ob','markersize',12,'linewidth',2);

corners2 = CornerDetect(I2, 50, smoothSTD, windowSize);
subplot(122); imshow(I2); hold on;
plot(corners2(1:nCorners,1),corners2(1:nCorners,2),'ob','markersize',12,'linewidth',2);
drawnow 

%%
R = prms.R;
SSDth = prms.SSDth;
nCorners = 10; 

% Corners returned by CornerDetect are ordered according to their response.
% So we can just use the top nCorners2 corners, instead of reruning CornerDetect.
corsSSD = naiveCorrespondanceMatching(I1, I2, corners1(1:nCorners,:), corners2(1:nCorners,:), R, SSDth);
showMatches(I1,I2,corners1(1:nCorners,1:2),corsSSD(:,3:4),corsSSD(:,1:2),corsSSD(:,3:4))
drawnow 

%%
F = fund(cor2,cor1);
figure; subplot(121); imshow(I1); hold on;
plot(cor1(:,1),cor1(:,2),'or','markersize',10,'linewidth',1.5);
for i = 1:size(cor1,1)
    pts = linePts(F*[cor2(i,:)';1], [1 size(I1,2)], [1 size(I1,1)]);
    plot(pts(:,1),pts(:,2),'linewidth',1.5)
end

subplot(122); imshow(I2); hold on;
plot(cor2(:,1),cor2(:,2),'or','markersize',10,'linewidth',1.5)
for i = 1:size(cor1,1)
    pts = linePts(F'*[cor1(i,:)';1], [1 size(I2,2)], [1 size(I2,1)]);
    plot(pts(:,1),pts(:,2),'linewidth',1.5)
end
drawnow 

%%
nCorners = 10;
R = prms.R;
SSDth = prms.SSDth;

corsSSD = correspondanceMatchingLine(I1, I2, corners1(1:nCorners,:), F, R, SSDth);
showMatches(I1,I2,corners1(1:nCorners,1:2),corsSSD(:,3:4),corsSSD(:,1:2),corsSSD(:,3:4))
drawnow 

%%
outlierTH = prms.outlierTH;
nCorners = 50;

corsSSD = correspondanceMatchingLine(I1, I2, corners1(1:nCorners,:), F, R, SSDth);
points3D = triangulate(corsSSD, P1, P2);
[ inlier, outlier ] = findOutliers(points3D, P2, outlierTH, corsSSD);

figure;imshow(I2); hold on
proj = P2*[points3D ones(size(points3D,1),1)]';
proj = bsxfun(@rdivide,proj(1:2,:),proj(3,:))';
plot(corsSSD(:,3),corsSSD(:,4),'ko','markersize',10,'linewidth',1.5);
plot(proj(inlier,1),proj(inlier,2),'+b','markersize',10,'linewidth',1.5);
plot(proj(outlier,1),proj(outlier,2),'+r','markersize',10,'linewidth',1.5);

showMatches(I1,I2,corners1(1:nCorners,1:2),corsSSD(inlier(1:20),3:4),corsSSD(inlier(1:20),1:2),corsSSD(inlier(1:20),3:4))
showMatches(I1,I2,corners1(1:nCorners,1:2),corsSSD(outlier,3:4),corsSSD(outlier,1:2),corsSSD(outlier,3:4))

