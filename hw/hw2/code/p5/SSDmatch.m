function SSD = SSDmatch(wind1,wind2)
    SSD = sum((wind1(:)-wind2(:)).^2);