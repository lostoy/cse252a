function showMatches(I1,I2,corners1,corners2,cornMatch1,cornMatch2)
I1sz = size(I1);
I2sz = size(I2);
Isz = [max(I1sz(1),I2sz(1)) I1sz(2)+I2sz(2) 3];
I = zeros(Isz);
I(1:I1sz(1),1:I1sz(2),:) = I1;
I(1:I2sz(1),I1sz(2)+1:end,:) = I2;

figure;imshow(I);hold on;
if ~isempty(corners1)
    plot(corners1(:,1),corners1(:,2),'or','markersize',10,'linewidth',2);
end
if ~isempty(corners2)
    plot(I1sz(2)+corners2(:,1),corners2(:,2),'or','markersize',10,'linewidth',2);
end
for i = 1:size(cornMatch1,1)
    plot([cornMatch1(i,1);I1sz(2)+cornMatch2(i,1)],[cornMatch1(i,2);cornMatch2(i,2)],'linewidth',1.5)
end