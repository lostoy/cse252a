function [ rect_list ] = draw_detect( img, img_map, w, h, number, gt_list)

    [~, sort_ind] = sort(img_map(:), 'descend');
    figure
    imshow(uint8(img))
    rect_list = zeros(number, 4);
    for i=1:number
        [oi, oj] = ind2sub(size(img_map), sort_ind(i));
        rect_list(i, :) = cat(2, [oj, oi] - [w, h]/2, [w, h]);
        rectangle('Position', rect_list(i, :), 'edgecolor', 'b', 'linewidth', 4)
    end
    
    if exist('gt_list', 'var')
        
        n_gt = size(gt_list, 1);
        for i=1:n_gt
            for j=1:number
                fprintf('iou_gt%d_rect%d: %.2f\n', i, j, IoU(gt_list(i, :), rect_list(j, :)))
            end
            rectangle('Position', gt_list(i, :), 'edgecolor', 'r', 'linewidth', 4)
        end
        
    end
end

