function [ output ] = IoU( gt, rect )
% bounding box: rec: [x, y, w, h]
gt_x1 = gt(1);
gt_y1 = gt(2);
gt_x2 = gt_x1 + gt(3) - 1;
gt_y2 = gt_y1 + gt(4) - 1;

det_x1 = rect(1);
det_y1 = rect(2);
det_x2 = det_x1 + rect(3) - 1;
det_y2 = det_y1 + rect(4) - 1;

intersect_area = seg_intersect(gt_x1, gt_x2, det_x1, det_x2) * seg_intersect(gt_y1, gt_y2, det_y1, det_y2);
union_area = gt(3)*gt(4) + rect(3)*rect(4) - intersect_area;
output = intersect_area / union_area;

end

function output = seg_intersect(l1, r1, l2, r2)
    if r1<=l2 || l1 >=r2
        output = 0;
        return
    end
    
    
    if l1<l2
        l = l2;
    else
        l = l1;
    end
    
    if r1>r2
        r = r2;
    else
        r = r1;
    end
    
    output = r-l;
    
end
