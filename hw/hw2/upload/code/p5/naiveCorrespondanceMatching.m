function corsSSD = naiveCorrespondanceMatching(I1, I2, corners1, corners2, R, SSDth)
corsSSD = zeros(size(corners1,1),4)*nan;
for i = 1:size(corners1,1)
    % For each corner in 1st image, compute SSD with all in 2nd image
    SSDscore = zeros(1,size(corners2,1));
    for j = 1:size(corners2,1)
        wind1 = I1(corners1(i,2)-R:corners1(i,2)+R,...
                   corners1(i,1)-R:corners1(i,1)+R,:);
        wind2 = I2(corners2(j,2)-R:corners2(j,2)+R,...
                   corners2(j,1)-R:corners2(j,1)+R,:);
        
        SSDscore(j) = SSDmatch(wind1,wind2);
    end
    % Find best match
    [minSSD,idx] = min(SSDscore);
    if minSSD <= SSDth
        corsSSD(i,:) = [corners1(i,:) corners2(idx,:)];
    end
end
corsSSD(isnan(corsSSD(:,1)),:) = [];







