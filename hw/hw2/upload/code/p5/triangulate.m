function points3D = triangulate(corsSSD, P1, P2)
points3D = zeros(size(corsSSD,1),3);
for i = 1:size(corsSSD,1)
    p1 = corsSSD(i,1:2);
    p2 = corsSSD(i,3:4);
    
    p1x = [0 -1 p1(2); 1 0 -p1(1); -p1(2) p1(1) 0];
    p2x = [0 -1 p2(2); 1 0 -p2(1); -p2(2) p2(1) 0];
    
    A = [p1x*P1;p2x*P2];
    [~,~,V] = svd(A);
    P = V(:,end);
    points3D(i,:) = P(1:3)/P(4);
end