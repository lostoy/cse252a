function [ car_map] = car_detect( car_tmp, car_img, tmp_size )

% resize the car image, assume small_size is h
img_size = 360;
car_img_resized = imresize(car_img, img_size/size(car_img, 1)); 

car_map = -1e100*zeros(size(car_img_resized));
for flip = 0:1
    car_tmp_resized = imresize(car_tmp, tmp_size);
    if flip
        car_tmp_resized = car_tmp_resized(:, end:-1:1);
    end
    
    car_map_tmp = matching_filter(car_tmp_resized, car_img_resized, 1);
    car_map = max(car_map_tmp, car_map);

end

car_map = imresize(car_map, size(car_img));
end

