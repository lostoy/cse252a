%%
close all
clc
clear

data_dir = '../../data/hw2_data';
output_dir = '../../data/output/p4';

%% 4.1
filter = double(imread([data_dir '/filter.jpg']));
toy = double(imread([data_dir '/toy.png']));

fprintf('------doing dectection for toys...\n')
toy_map = matching_filter(filter, toy, 0);
imagesc(toy_map)
saveas(gcf, [output_dir '/toy_map.eps'], 'epsc')

rect_list = draw_detect(toy, toy_map, size(filter, 2), size(filter, 1), 3);

saveas(gcf, [output_dir '/toy_detection.eps'], 'epsc')

%% 4.2
gt_list = zeros(3, 4);
gt_list(1, :) = [95, 140, 102, 88];
gt_list(2, :) = [81, 125, 138, 125];
gt_list(3, :) = [55, 104, 196, 161];

fprintf('------doing IoU computing for toys...\n')
draw_detect(toy, toy_map, size(filter, 2), size(filter, 1), 1, gt_list);
saveas(gcf, [output_dir '/toy_IoU.eps'], 'epsc')

%% 4.3

h_list = [100, 150, 100, 120, 100];
ratio_list = [0.3, 0.4, 1.0, 0.4, 0.4];
for i=1:5
    car_tmp = double(imread([data_dir '/cartemplate.jpg']));
    car_tmp = car_tmp(end:-1:1, end:-1:1);
    car_img = double(imread(sprintf('%s/car%d.jpg', data_dir, i)));
    
    t_gt = importdata(sprintf('%s/car%d.txt', data_dir, i));
    t_gt = t_gt';
    t_gt = t_gt(:);
    
    gt = zeros(1, 4);
    gt(1:2) = t_gt(1:2);
    gt(3) = t_gt(3) - t_gt(1) + 1;
    gt(4) = t_gt(4) - t_gt(2) + 1;
    
    %
    h = h_list(i);
    w = h/ratio_list(i);
    
    fprintf('------doing detection for car%d...\n', i)
    [car_map] = car_detect(car_tmp, car_img, [h, w]);
    
    figure
    imagesc(car_map)
    saveas(gcf, sprintf('%s/car_map%d.eps', output_dir, i), 'epsc');
    
    rect_list = draw_detect(car_img, car_map, w, h, 1, gt);
    saveas(gcf, sprintf('%s/car_detection%d.eps', output_dir, i), 'epsc');
end
