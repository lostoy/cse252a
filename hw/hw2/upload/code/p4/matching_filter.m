function [ img_filtered ] = matching_filter( filter, img, normalize )
filter_demean = filter - mean(filter(:));
img_demean = img - mean(img(:));
img_filtered = conv2(img_demean, filter_demean, 'same');

if normalize
    img_filtered = img_filtered / numel(filter);
end
end

