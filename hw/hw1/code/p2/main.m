%% data preparation(column-major)
clear; clc;close all
addpath('../../data/hw1_data')
output_dir = '../../data/output/p2/';
point_A_list = [-1, -0.5, 2; 1, -0.5, 2; 1, 0.5, 2; -1, 0.5, 2]';
%% p2.1
f = 1;
rot = [0, 0, 1]*0;
R = rot_mat_from_axis_angle(rot);
t = [0, 0, 0]';
K = intr_mat(f);

E = [R, t; zeros(1, 3), 1];
disp('E=')
disp(E)
disp('K=')
disp(K)
point_B_list = zeros(3, 4);

for i=1:4
    point_B_homo = K * (R * point_A_list(:, i) + t);
    point_B_list(:, i) = point_B_homo ;%/ point_B_homo(3);
end
subplot(2,2,1)
plotsquare(point_B_list)
title('proj,origin')
xlim([-0.5 0.5])
ylim([-0.5 0.5])
%% p2.2
f = 1;
rot = [0, 0, 1]*0;
R = rot_mat_from_axis_angle(rot);
t = [0, 0, 1]';
K = intr_mat(f);

E = [R, t; zeros(1, 3), 1];
disp('E=')
disp(E)
disp('K=')
disp(K)
point_B_list = zeros(3, 4);

for i=1:4
    point_B_homo = K * (R * point_A_list(:, i) + t);
    point_B_list(:, i) = point_B_homo ;%/ point_B_homo(3);
end
subplot(2,2,2)
plotsquare(point_B_list)
title('proj,translation')

%% p2.3
f = 1;
rot1 = [0, 0, 1]*60/180*pi;
R1 = rot_mat_from_axis_angle(rot1);

rot2 = [0, 1, 0]*45/180*pi;
R2 = rot_mat_from_axis_angle(rot2);

R = R2*R1;

t = [0, 0, 1]';
K = intr_mat(f);

E = [R, t; zeros(1, 3), 1];
disp('E=')
disp(E)
disp('K=')
disp(K)
point_B_list = zeros(3, 4);

for i=1:4
    point_B_homo = K * (R * point_A_list(:, i) + t);
    point_B_list(:, i) = point_B_homo ;%/ point_B_homo(3);
end
subplot(2,2,3)
plotsquare(point_B_list)
title('proj,rotation')
%% p2.4
f = 15;
rot1 = [0, 0, 1]*60/180*pi;
R1 = rot_mat_from_axis_angle(rot1);

rot2 = [0, 1, 0]*90/180*pi;
R2 = rot_mat_from_axis_angle(rot2);

R = R2*R1;

t = [0, 0, 13]';
K = intr_mat(f);

E = [R, t; zeros(1, 3), 1];
disp('E=')
disp(E)
disp('K=')
disp(K)
point_B_list = zeros(3, 4);

for i=1:4
    point_B_homo = K * (R * point_A_list(:, i) + t);
    point_B_list(:, i) = point_B_homo ;%/ point_B_homo(3);
end
subplot(2,2,4)
plotsquare(point_B_list)
title('proj,rotation long distance')