function rot_mat = rot_mat_from_axis_angle(n)
n_norm = norm(n);
if n == 0
    rot_mat = eye(3);
    return
end

rot_mat = eye(3);
for i=1:3
    rot_mat(:, i) = rot_point_around_axis_angle(n, rot_mat(:, i));
end

end


function p_out = rot_point_around_axis_angle(n, p_in)
    if norm(p_in) ~= 1.0
        error('expect to work with unit vector now!')
    end
    
    angle = norm(n);
    
    if angle == 0
        p_out = p_in;
        return
    end
    n_norm = n / angle;
    
    y_hat = cross(n, p_in);
    if norm(y_hat) == 0
        p_out = p_in;
        return
    end
    
    y_hat = y_hat / norm(y_hat);
    
    x_hat = cross(y_hat, n_norm);
    
    p_in_x = dot(p_in, x_hat);
    p_in_n = dot(p_in, n_norm);
    p_out = p_in_n * n_norm + cos(angle)*p_in_x*x_hat + sin(angle)* ...
            p_in_x*y_hat;
    
end

