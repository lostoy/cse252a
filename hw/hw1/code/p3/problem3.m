clear all
close all
clc

load facedata

%% Part 1
figure;
subplot(121);imagesc(uniform_albedo); colormap('gray'); daspect([1 1 1]);
set(gca,'xtick',[],'ytick',[])
subplot(122);imagesc(albedo); colormap('gray'); daspect([1 1 1]);
set(gca,'xtick',[],'ytick',[])

%% Part 2
figure;
subplot(121);
surf(heightmap,uniform_albedo,'EdgeColor','none','LineStyle','none'); 
colormap('gray'); daspect([1 1 1]); axis([1 125 1 188 -10 300]); 
caxis([0 1.1]); set(gca,'xtick',[],'ytick',[],'ztick',[]); 
subplot(122);surf(heightmap,albedo,'EdgeColor','none','LineStyle','none'); 
colormap('gray'); daspect([1 1 1]); axis([1 125 1 188 -10 300])
set(gca,'xtick',[],'ytick',[],'ztick',[]);

%% Part 3
normal = compute_surface_normal(heightmap);

[x,y] = meshgrid(1:size(heightmap,2),1:size(heightmap,1));
idx_x = 1:4:size(normal,1);
idx_y = 1:4:size(normal,2);

figure;
surf(x,y,heightmap,albedo,'EdgeColor','none','LineStyle','none')
hold on
quiver3(x(idx_x,idx_y),y(idx_x,idx_y),heightmap(idx_x,idx_y),...
    normal(idx_x,idx_y,1),normal(idx_x,idx_y,2),normal(idx_x,idx_y,3));
colormap('gray'); daspect([1 1 1]);
set(gca,'xtick',[],'ytick',[],'ztick',[]); axis([1 125 1 188 -10 300])

%% Part 4
figure

I1_unif = generate_image(lightsource(1,:),heightmap,normal,uniform_albedo);
subplot(231); imagesc(I1_unif); colormap('gray'); daspect([1 1 1]);
set(gca,'xtick',[],'ytick',[]);
hold on; plot(lightsource(1,1),lightsource(1,2),'y*')
I2_unif = generate_image(lightsource(2,:),heightmap,normal,uniform_albedo);
subplot(232); imagesc(I2_unif); colormap('gray'); daspect([1 1 1]);
set(gca,'xtick',[],'ytick',[])
hold on; plot(lightsource(2,1),lightsource(2,2),'y*')
I12_unif = generate_image(lightsource,heightmap,normal,uniform_albedo);
subplot(233); imagesc(I12_unif); colormap('gray'); daspect([1 1 1]);
set(gca,'xtick',[],'ytick',[])
hold on; plot(lightsource(:,1),lightsource(:,2),'y*')

I1 = generate_image(lightsource(1,:),heightmap,normal,albedo);
subplot(234); imagesc(I1); colormap('gray'); daspect([1 1 1]);
set(gca,'xtick',[],'ytick',[])
hold on; plot(lightsource(1,1),lightsource(1,2),'y*')
I2 = generate_image(lightsource(2,:),heightmap,normal,albedo);
subplot(235); imagesc(I2); colormap('gray'); daspect([1 1 1]);
set(gca,'xtick',[],'ytick',[])
hold on; plot(lightsource(2,1),lightsource(2,2),'y*')
I12 = generate_image(lightsource,heightmap,normal,albedo);
subplot(236); imagesc(I12); colormap('gray'); daspect([1 1 1]);
set(gca,'xtick',[],'ytick',[])
hold on; plot(lightsource(:,1),lightsource(:,2),'y*')

