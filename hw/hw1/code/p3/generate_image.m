function img = generate_image(lightsource,heightmap,normal,albedo)
nSrc = size(lightsource,1);

[x,y] = meshgrid(1:size(heightmap,2),1:size(heightmap,1));
surface = cat(3,x,y,heightmap);

src = cell(1,nSrc);
dist = cell(1,nSrc);
img = zeros(size(heightmap));
for i = 1:nSrc
    src{i} = bsxfun(@minus, reshape(lightsource(i,:),[1,1,3]), surface);
    dist{i} = sqrt(sum(src{i}.^2,3));
    src{i} = bsxfun(@rdivide,src{i},dist{i});
    
    img = img + albedo .* max(sum(normal.*src{i},3),0) ./ dist{i};
end
