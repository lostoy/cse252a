function normal = compute_surface_normal(surface)
dx_kernel = [-1 0 1]/2;
dx = imfilter(surface,dx_kernel);
dy_kernel = [-1;0;1]/2;
dy = imfilter(surface,dy_kernel);

normal = cat(3,-dx,-dy,ones(size(dx)));
normal = bsxfun(@rdivide,normal,sqrt(sum(normal.^2,3)));