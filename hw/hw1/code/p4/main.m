%% load data and config
clear
clc
close all
output_dir = '../../data/output/p4';
load('../../data/hw1_data/synthetic_data.mat')

%% with 3 imgs or 4 imgs
for all_img=[0, 1]
    % forming linear equation
    b1 = double(im1(:));
    b2 = double(im2(:));
    b3 = double(im3(:));
    b4 = double(im4(:));
    S = -[l1; l3; l4];
    b = [b1'; b3'; b4'];

    if all_img == 1
        S = [S; -l2];
        b = [b; b2'];
    end
    
    % solve
    N = S \ b;
    
    % normalize to get surface normal and albedo
    albedo = sqrt(sum(N.^2, 1));
    albedo_map = reshape(albedo, size(im1));
    N_hat = bsxfun(@times, N, 1./albedo);
    N_hat_map = reshape(N_hat, [3, size(im1)]);
    
    %% albedo visualization
    figure
    imshow((albedo_map-min(albedo))./(max(albedo) - min(albedo)))
    saveas(gcf, [output_dir '/albedo_' num2str(all_img) '.eps'], 'epsc')
    %% integral
    p = squeeze(-N_hat_map(2,:,:) ./ N_hat_map(3,:,:));
    q = squeeze(-N_hat_map(1,:,:) ./ N_hat_map(3,:,:));
    depth0 = cumsum(p(1, :));
    depth = cumsum([depth0; q], 1);
    depth = depth(1:size(im1, 1), 1:size(im1,2));
    depth_horn = integrate_horn2(p, q, ones(size(im1)), 1000, 0);
    figure
    surf(depth)
    saveas(gcf, [output_dir '/depth_' num2str(all_img) '.eps'], 'epsc')
    figure
    surf(depth_horn)
    saveas(gcf, [output_dir '/depth_horn_' num2str(all_img) '.eps'], 'epsc')
    %% normal visualization
    step = 5;
    x_grid = 1:step:size(im1,1);
    y_grid = 1:step:size(im1,2);
    [X, Y] = meshgrid(x_grid, y_grid);
    Z = depth(x_grid, y_grid);
    figure
    quiver3(X, Y, Z, squeeze(N_hat_map(2, x_grid, y_grid)), ...
        squeeze(N_hat_map(1, x_grid, y_grid)), squeeze(N_hat_map(3, x_grid, y_grid)))
    saveas(gcf, [output_dir '/normal_' num2str(all_img) '.eps'], 'epsc')
end