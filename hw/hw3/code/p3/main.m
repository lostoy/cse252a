%%
data_dir ='../../hw3_data/flower';
output_dir = '../../data/output/p3/';
%%
close all
im0 = im2double((imread([data_dir '/00029.png'])));
im1 = im2double((imread([data_dir '/00030.png'])));

if size(im0,3)~=1
    im0 = rgb2gray(im0);
    im1 = rgb2gray(im1);
end
h = size(im0, 1);
w = size(im0, 2);

step = 3.5;
[X, Y] = meshgrid(1:step:w, 1:step:h);
p0 = [X(:) Y(:)]';

n_iter = 30;
win_size_list = [5, 10, 15, 20, 25];

n_level_list = [1, 2, 3, 4];
min_eig = 1e-4;
for win_size = win_size_list
    for n_level = n_level_list
        [ p1, valid_mask ]=LK_pyrd_optical_flow(im0, im1, p0, n_iter, win_size, n_level, min_eig);
        ind = valid_mask == 1;
        dp = p1-p0;
        
        quiver(p0(1,ind)/step, p0(2,ind)/step, dp(1,ind), dp(2,ind))
        xlim([1,w]/step)
        ylim([1,h]/step)
        axis ij
        saveas(gcf, [output_dir, '/win_size' num2str(win_size), 'n_level' num2str(n_level), 'n_iter', num2str(n_iter), 'min_eig', num2str(min_eig), '.eps'], 'epsc');
        close all
        
    end
end

