clear 
clc
%%
close all
I1 = imread('../../hw3_data/corridor/bt.000.png');
I1 = im2double(I1);
I2 = imread('../../hw3_data/corridor/bt.001.png');
I2 = im2double(I2);

for w = [15 30 100]
    windowSize = w;
    tau = 0.001;
    maxDisp = 6;
    [u, v, hitMap] = opticalFlow(I1, I2, windowSize, tau, maxDisp);

    figure; subplot(211)
    imshow(I1); hold on
    h = imshow(hitMap);
    set(h, 'AlphaData', 0.5);
    subplot(212)
    [x,y] = meshgrid(1:20,1:20);
    u2 = u(round(linspace(1,size(u,1),20)),round(linspace(1,size(u,2),20)));
    v2 = v(round(linspace(1,size(u,1),20)),round(linspace(1,size(u,2),20)));
    idx = ~isnan(u2);
    [y2,x2] = find(idx);
    quiver(x2,y2,u2(idx),v2(idx)); axis ij; daspect([1 1 1]); axis([1 20 1 20])
end

windowSize = 100;
tau = 0.001;
maxDisp = 7;
[u, v, hitMap] = opticalFlow(I1, I2, windowSize, tau, maxDisp);
corners = CornerDetect(I1,50,1,7);

figure
subplot(121)
imshow(I1); hold on
plot(corners(:,1),corners(:,2),'or','linewidth',2,'markersize',6)
subplot(122); 
c2 = floor((corners-1)/maxDisp)+1;
u2 = diag(u(c2(:,2),c2(:,1)));
v2 = diag(v(c2(:,2),c2(:,1)));
imshow(I1); hold on
plot(corners(:,1),corners(:,2),'or','linewidth',2,'markersize',6)
q = quiver(corners(:,1),corners(:,2),u2*maxDisp,v2*maxDisp,'linewidth',2); 
axis ij; daspect([1 1 1])

%%

close all
I1 = imread('../../hw3_data/synth/synth_000.png');
I1 = im2double(I1);
I2 = imread('../../hw3_data/synth/synth_001.png');
I2 = im2double(I2);

for w = [15 30 100]
    windowSize = w;
    tau = 0.001;
    maxDisp = 1;
    [u, v, hitMap] = opticalFlow(I1, I2, windowSize, tau, maxDisp);

    figure; subplot(211)
    imshow(I1); hold on
    h = imshow(hitMap);
    set(h, 'AlphaData', 0.5);
    subplot(212)
    u2 = u(round(linspace(1,size(u,1),20)),round(linspace(1,size(u,2),20)));
    v2 = v(round(linspace(1,size(u,1),20)),round(linspace(1,size(u,2),20)));
    idx = ~isnan(u2);
    [y2,x2] = find(idx);
    quiver(x2,y2,u2(idx),v2(idx)); axis ij; daspect([1 1 1]); axis([1 20 1 20])
end

windowSize = 15;
tau = 0.001;
maxDisp = 1;
[u, v, ~] = opticalFlow(I1, I2, windowSize, tau, maxDisp);
corners = CornerDetect(I1,50,1,7);

figure
subplot(121)
imshow(I1); hold on
plot(corners(:,1),corners(:,2),'or','linewidth',2,'markersize',6)
subplot(122); 
c2 = floor((corners-1)/maxDisp)+1;
u2 = diag(u(c2(:,2),c2(:,1)));
v2 = diag(v(c2(:,2),c2(:,1)));
imshow(I1); hold on
plot(corners(:,1),corners(:,2),'or','linewidth',2,'markersize',6)
quiver(corners(:,1),corners(:,2),u2*maxDisp,v2*maxDisp,'linewidth',2); 
axis ij; daspect([1 1 1])


%%

close all
I1 = imread('../../hw3_data/sphere/sphere.0.png');
I1 = im2double(rgb2gray(I1));
I2 = imread('../../hw3_data/sphere/sphere.1.png');
I2 = im2double(rgb2gray(I2));

for w = [15 30 100]
    windowSize = w;
    tau = 0.001;
    maxDisp = 1;
    [u, v, hitMap] = opticalFlow(I1, I2, windowSize, tau, maxDisp);

    figure; subplot(211)
    imshow(I1); hold on
    h = imshow(hitMap);
    set(h, 'AlphaData', 0.5);
    subplot(212)
    [x,y] = meshgrid(1:20,1:20);
    u2 = u(round(linspace(1,size(u,1),20)),round(linspace(1,size(u,2),20)));
    v2 = v(round(linspace(1,size(u,1),20)),round(linspace(1,size(u,2),20)));
    idx = ~isnan(u2);
    [y2,x2] = find(idx);
    quiver(x2,y2,u2(idx),v2(idx)); axis ij; daspect([1 1 1]); axis([1 20 1 20])
end

windowSize = 15;
tau = 0.0001;
maxDisp = 1;
[u, v, ~] = opticalFlow(I1, I2, windowSize, tau, maxDisp);
corners = CornerDetect(I1,50,0.3,7);

figure
subplot(121)
imshow(I1); hold on
plot(corners(:,1),corners(:,2),'or','linewidth',2,'markersize',6)
subplot(122); 
c2 = floor((corners-1)/maxDisp)+1;
u2 = diag(u(c2(:,2),c2(:,1)));
v2 = diag(v(c2(:,2),c2(:,1)));
imshow(I1); hold on
plot(corners(:,1),corners(:,2),'or','linewidth',2,'markersize',6)
q = quiver(corners(:,1),corners(:,2),u2*maxDisp,v2*maxDisp,'linewidth',2); 
axis ij; daspect([1 1 1])


