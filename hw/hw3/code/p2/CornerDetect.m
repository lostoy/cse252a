function corners = CornerDetect(Image, nCorners, smoothSTD, windowSize)
if mod(windowSize,2)==0, error('windowSize must be an odd integer.'); end
if size(Image,3) == 3
    Igray = rgb2gray(Image);
else
    Igray = Image;
end
Isz = size(Igray);

% Smoothing and taking derivatives
if mod(round(5*smoothSTD),2)==0
    Gsz = round(5*smoothSTD)+1;
else
    Gsz = round(5*smoothSTD);
end
G = fspecial('gauss',[Gsz Gsz], smoothSTD);
[Gx,Gy] = gradient(G);
Ix = imfilter(Igray,Gx);
Iy = imfilter(Igray,Gy);

% Computing corner responses
E = zeros(size(Igray));
Wsz = windowSize;
for i = (Gsz+1)/2:Isz(1)-(Gsz-1)/2-Wsz+1
    for j = (Gsz+1)/2:Isz(2)-(Gsz-1)/2-Wsz+1
        Ix_wind = Ix(i:i+Wsz-1,j:j+Wsz-1);
        Iy_wind = Iy(i:i+Wsz-1,j:j+Wsz-1);
        C = [Ix_wind(:)'*Ix_wind(:) Ix_wind(:)'*Iy_wind(:);
             Ix_wind(:)'*Iy_wind(:) Iy_wind(:)'*Iy_wind(:)];
         E(i+(Wsz-1)/2,j+(Wsz-1)/2) = min(eig(C));
    end
end

% Non-Max Supression
Emax = imdilate(E, true(Wsz)); % Max filter
Esup = zeros(size(Igray));
Esup(Emax(:)==E(:)) = E(Emax(:)==E(:));

% Find strongest responses
[~,ord] = sort(Esup(:),'descend');
[x,y] = meshgrid(1:Isz(2),1:Isz(1));
corners = [x(ord(1:nCorners)) y(ord(1:nCorners))];


