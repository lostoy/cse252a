%% preset
clear
clc
close all
data_path = '../../hw3_data/stadium.jpg';
output_data_dir = '../../data/output/p1/';
if ~exist(output_data_dir)
    mkdir(output_data_dir)
end

img = imread(data_path);

%% H estimation
points_list = zeros(3,4,3);
points_list(:,:,1) = [169, 199, 1;
                275, 203, 1;
                166, 225, 1;
                268, 241, 1;]';

points_list(:,:,2) = [275, 202, 1;
                403, 210, 1;
                268, 240, 1;
                397, 259, 1;]';
            
points_list(:,:,3) = [118, 196, 1;
                170, 199, 1;
                115, 217, 1;
                164, 226, 1;]';
w = 256;
h = 64;
new_points = [1, 1, 1;
              w, 1, 1;
              1, h, 1;
              w,h,1;]';
          

for i=1:3
    points = points_list(:,:,i);
    [H, iH] = computeH(points, new_points);
    warped_img = warp(img, new_points, iH);
    figure
    imshow(warped_img);
    saveas(gcf,[output_data_dir '/ad' num2str(i) '.eps'], 'epsc');
end