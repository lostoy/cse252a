function [ H, iH] = computeH( points, new_points )
% computes H to map point->new_point
[H1, iH1] = compute_H_to_e(points);
[H2, iH2] = compute_H_to_e(new_points);

H = iH2 * H1;
iH = iH1 * H2;
end

function [H, iH] = compute_H_to_e(points)
% input: points is 3*4, the first three cols coressponds to eye(3)
lambda = points(:, 1:3) \ points(:, 4);
iH = points(:, 1:3) * diag(lambda);
H = pinv(iH);
end
