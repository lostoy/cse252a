\documentclass{assignment}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{hyperref}
\usepackage{listings}
\usepackage{subcaption}
\lstset{
numbers=left
}

\coursetitle{Computer Vision I}
\courselabel{CSE 252A}
\exercisesheet{Homework 3}{}
\student{Li, Yingwei and Morgado, Pedro}
\university{University of California, San Diego}
\semester{Fall 2015}
\date{\today}


\newcommand{\p}{\mathbf{p}}
\newcommand{\x}{\mathbf{x}}
\newcommand{\bP}{\mathbf{P}}

\begin{document}
\begin{problemlist}
% problem 1
\pbitem {\it Image warping and merging}

Following pages 316-318 in Introductory Techniques for 3-D Computer Vision by Trucco and Verri, given four points $p_1,p_2,p_3, p_4$, a projective transformation $H_{p,e}$ could be estimated to transform $p_i$ to $e_i$, where $e_i$ is the canonical bases. Then the required transformation to transform $p_i$ to $p'_i$ could be obtained by:$H=H_{p',e}^{-1} H_{p,e}$.

After getting the transformation matrix $H$, to get the warped image, every target pixel $p'$, is mapped back to its corresponding location $p$ in the original image, and bilinear interpolated. See attached code for details.

Results for three ads board from the {\it stadium.jpg} is shown in Fig.~\ref{fig:p1:stadium}.

\begin{figure}[h!]
	\centering
	\begin{subfigure}[b]{0.3 \textwidth}
		\includegraphics[width=1\textwidth]{../data/output/p1/ad1.eps}
		\caption{ad1}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.3 \textwidth}
		\includegraphics[width=1\textwidth]{../data/output/p1/ad2.eps}
		\caption{ad2}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.3 \textwidth}
		\includegraphics[width=1\textwidth]{../data/output/p1/ad3.eps}
		\caption{ad3}
	\end{subfigure}
	\caption{Warped ads board}
	\label{fig:p1:stadium}
\end{figure}

\pbitem {\it Optical Flow}
\paragraph{Part 1 - Dense Optical Flow:} In this problem, we implemented the single scale Lucas-Kanade algorithm for optical flow estimation. The results for the corridor, synthetic and sphere images are shown in Fig.~\ref{fig:p2:corridor}, \ref{fig:p2:synth} and \ref{fig:p2:sphere}. This was accomplished by minimizing the sum-squared error of the brightness constancy equations for each pixel in
a window. The optical flow was only computed at pixels for which the minimum eigenvalue of the matrix $A^TA$ is large enough (i.e. at pixels for which the matrix in not close to singular). This ensures that we are computing the optical flow in windows for which it is possible to track its movement along both spatial directions. Edges, whose displacements can only be tracked along one dimension, will have one eigenvalue small and one large, and flat regions will have both eigenvalues small. Therefore, by thresholding the smallest eigenvalue, we are avoiding computing the optical flow on regions where the displacements are impossible to track. Finally, because the optical flow constraint equation assumes small displacements (ideally subpixel displacements) we added another parameter representing the maximum expected displacement. This is then used to rescale the image, so that the subpixel displacement assumption is met.

In the corridor image (Fig.~\ref{fig:p2:synth}), we can see that as the window size gets larger, two different effects are observed. One one hand, the valid regions become larger. This happens because as the window increases, it is bound to contain more details and not just the plain regions on the wall or just edges. With a 100$\times$100 window, most image is indeed valid. On the other hand, the estimated flow fields are more uniform. This happens because the flow fields at each point is computed using more constrains (each pixel within the window acts as a constrain since it is assumed to have the same displacement vector). 
For example, for the 15$\times$15 window, we get poor estimates on the corridor image at certain pixels, which are then corrected when using larger windows. However, by using large windows, we cannot accurately estimate the flow field on small regions of the image, since all the other pixels in the window, for which the true optimal field is different, introduces an error on the estimation of the optical field of the center pixel. This is evident on Fig.~\ref{fig:p2:sphere}, where the flow field for the backside of the sphere is not accurately estimated when using a window size of 100. Finally, on the synthetic images (Fig.~\ref{fig:p2:synth}), the flow field computations are valid on the entire image, even for the 15 window size. This is because the fluctuations of intensity in the image are detectable within 15 pixels. The camera in this case seems to be just rotating.

\begin{figure}[t!]
	\centering
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[height=12cm,trim={80 0 80 0},clip]{images/corridor15.png}
		\caption{15}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[height=12cm,trim={80 0 80 0},clip]{images/corridor30.png}
		\caption{30}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[height=12cm,trim={80 0 80 0},clip]{images/corridor100.png}
		\caption{100}
	\end{subfigure}
	\caption{Dense optimal flow for corridor picture.}
	\label{fig:p2:corridor}
\end{figure}


\begin{figure}[t!]
	\centering
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[height=12cm,trim={80 0 80 0},clip]{images/synth15.png}
		\caption{15}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[height=12cm,trim={80 0 80 0},clip]{images/synth30.png}
		\caption{30}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[height=12cm,trim={80 0 80 0},clip]{images/synth100.png}
		\caption{100}
	\end{subfigure}
	\caption{Dense optimal flow for synthetic picture.}
	\label{fig:p2:synth}
\end{figure}

\begin{figure}[t!]
	\centering
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[height=12cm,trim={80 0 80 0},clip]{images/sphere15.png}
		\caption{15}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[height=12cm,trim={80 0 80 0},clip]{images/sphere30.png}
		\caption{30}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[height=12cm,trim={80 0 80 0},clip]{images/sphere100.png}
		\caption{100}
	\end{subfigure}
	\caption{Dense optimal flow for synthetic picture.}
	\label{fig:p2:sphere}
\end{figure}

\paragraph{Part 2 - Corner Detection:} 
The corner detector algorithm from the previous assignment was used to detect 50 corner in the first image of each series. The results for the corridor, synthetic and sphere images are shown in Fig.~\ref{fig:p2:corridor_sparse}, \ref{fig:p2:synth_sparse} and \ref{fig:p2:sphere_sparse}. The detected corners are shown with red circles.

\paragraph{Part 3 - Sparse Optical Flow:} 
	Using the dense flow field estimator of part 1, the corner detection of part 2, and searching for a good set of parameter for each experiment, we extracted the sparse flow fields (i.e. the flow field estimated only at reliable corner positions). Results are shown in Fig.~\ref{fig:p2:corridor_sparse}, \ref{fig:p2:synth_sparse} and \ref{fig:p2:sphere_sparse}. A window size of 100 was used for the corridor and synthetic image pairs, but for the sphere the window size was set to 15. This enables us to obtain a better estimation over the entire sphere, even in the top where a small region has the flow in the opposite direction. $\tau$ was set to 0.001 for the corridor and synthetic images, and to 0.0001 for the sphere. This allow the estimation of the flow field in the middle of the sphere, where corners are not as well defined, but as the results show, the flow field estimation is still accurate.
	
	The focus of expansion (FOE) is observable when the camera is moving forward as is the case in the corridor image. In this case, all velocity vectors (if perfectly estimated) should meet at the FOE. This criteria was used to approximately mark the FOE in Fig.~\ref{fig:p2:corridor_sparse}. In the synthetic image series, the camera seems to be rotating and, in the sphere images, the camera is static. So there is no FOE in these two series.

\begin{figure}[t!]
	\centering
	\includegraphics[height=5cm,trim={80 20 80 20},clip=true]{images/corridor_sparse.png}
	\caption{Sparse optimal flow for corridor picture.}
	\label{fig:p2:corridor_sparse}
\end{figure}

\begin{figure}[t!]
	\centering
	\includegraphics[height=5cm,trim={80 20 80 20},clip=true]{images/synth_sparse.png}
	\caption{Sparse optimal flow for synthetic picture.}
	\label{fig:p2:synth_sparse}
\end{figure}

\begin{figure}[t!]
	\centering
	\includegraphics[height=5cm,trim={80 20 80 20},clip=true]{images/sphere_sparse.png}
	\caption{Sparse optimal flow for synthetic picture.}
	\label{fig:p2:sphere_sparse}
\end{figure}

% problem 3
\pbitem {\it Iterative Coarse to Fine Optical Flow}

We implemented the iterative coarse to fine optical flow algorithm and set the window size to 5, 15 and 25 to get the flow field shown in Fig.\ref{fig:p3:flow}. $min_{eig}$ is set to 1e-4 and the $n_{iter}$ is set to 3 in all the experiments.

\begin{figure}[t!]
	\centering
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=1\textwidth]{{../data/output/p3/win_size5n_level1n_iter3min_eig0.0001}.eps}
		\caption{pyramid=1, win=5}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=1\textwidth]{{../data/output/p3/win_size15n_level1n_iter3min_eig0.0001}.eps}
		\caption{pyramid=1, win=15}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=1\textwidth]{{../data/output/p3/win_size25n_level1n_iter3min_eig0.0001}.eps}
		\caption{pyramid=1, win=25}
	\end{subfigure}
	
	
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=1\textwidth]{{../data/output/p3/win_size5n_level4n_iter3min_eig0.0001}.eps}
		\caption{pyramid=4, win=5}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=1\textwidth]{{../data/output/p3/win_size15n_level4n_iter3min_eig0.0001}.eps}
		\caption{pyramid=4, win=15}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.3\textwidth}
		\includegraphics[width=1\textwidth]{{../data/output/p3/win_size25n_level4n_iter3min_eig0.0001}.eps}
		\caption{pyramid=4, win=25}
	\end{subfigure}
	
	\caption{Result of dense flow and iterative coarse to fine optical flow on flower sequence}
	\label{fig:p3:flow}
\end{figure}

By comparing results in the same column in Fig.\ref{fig:p3:flow}, we see that the dense optical flow algorithm(with pyramid level set to 1), struggles at regions with large displacement({\it etc} tree trunk) while the iterative coarse to fine optical flow algorithm handles these regions very well. 

In general, the coarse to fine algorithm is expected to do a better job when the depth of the scene is small or the movement is large. 

However, the coarse to fine algorithm is not always preferable. For example, in the last column of Fig.\ref{fig:p3:flow}, when the window size is large, the coarse to fine algorithm seems to make more mistakes at the region with flowers, while the vanilla dense optical flow algorithms performs well at such regions. This can be explained by the fact that, with a larger window size, the coarse to fine algorithm might match to a similar region that's far away from the correct match, especially when the region is highly textured, {\it etc} the flower region.


\end{problemlist}
\lstinputlisting[language=MATLAB, caption=computeH]{../code/p1/computeH.m}
\lstinputlisting[language=MATLAB, caption=warp]{../code/p1/warp.m}
\lstinputlisting[language=MATLAB, caption=entry point for problem 1]{../code/p1/main.m}

\lstinputlisting[language=MATLAB, caption=Dense optical flow]{../code/p2/opticalFlow.m}
\lstinputlisting[language=MATLAB, caption=entry point for problem 2]{../code/p2/main.m}

\lstinputlisting[language=MATLAB, caption=Iterative coarse to fine optical flow]{../code/p3/LK_pyrd_optical_flow.m}
\lstinputlisting[language=MATLAB, caption=entry point for problem 3]{../code/p3/main.m}

\end{document}
