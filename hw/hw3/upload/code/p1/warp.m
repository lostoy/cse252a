function [ warped_img ] = warp( I1, new_points, iH )
warped_w = new_points(1,2) - new_points(1,1) + 1;
warped_h = new_points(2,3) - new_points(2,2) + 1;

warped_img = zeros(warped_h, warped_w, 3);

[X, Y] = meshgrid(new_points(1,1):new_points(1,2), new_points(2,1):new_points(2,3));
tmpP = cat(1, permute(X, [3,1,2]), permute(Y, [3,1,2]), permute(ones(size(X)), [3,1,2]));
new_tmpP = reshape(iH * reshape(tmpP, 3, []), [3,size(X)]);
new_tmpP =bsxfun(@times, new_tmpP(1:2,:,:), 1 ./ new_tmpP(3,:,:));

for c=1:3
    warped_img(:,:,c) = interp2(double(I1(:,:,c)), squeeze(new_tmpP(1,:,:)), squeeze(new_tmpP(2,:,:)));
end
warped_img=uint8(warped_img);
end

