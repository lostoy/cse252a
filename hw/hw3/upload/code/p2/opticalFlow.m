function [u, v, hitMap] = opticalFlow(I1, I2, windowSize, tau, maxDisp)

I1sml = imresize(I1,1/maxDisp);
I2sml = imresize(I2,1/maxDisp);

Gx = [-1 0 1];
Gy = [-1;0;1];
Ix = imfilter(I1,Gx,'replicate');
Iy = imfilter(I1,Gy,'replicate');
wSz = windowSize;
iSz = size(I1);

IxSml = imfilter(I1sml,Gx,'replicate');
IySml = imfilter(I1sml,Gy,'replicate');
ItSml = I2sml-I1sml;
wSzSml = ceil(windowSize/maxDisp);
iSzSml = size(I1sml);

% Computing corner responses
E = zeros(iSz);
for i = 1:iSz(1)
    for j = 1:iSz(2)
        Ix_wind = Ix(max(i-floor(wSz/2),1):min(i-floor(wSz/2)+wSz-1,iSz(1)),max(j-floor(wSz/2),1):min(j-floor(wSz/2)+wSz-1,iSz(2)));
        Iy_wind = Iy(max(i-floor(wSz/2),1):min(i-floor(wSz/2)+wSz-1,iSz(1)),max(j-floor(wSz/2),1):min(j-floor(wSz/2)+wSz-1,iSz(2)));
        
        C = [Ix_wind(:)'*Ix_wind(:) Ix_wind(:)'*Iy_wind(:);
             Ix_wind(:)'*Iy_wind(:) Iy_wind(:)'*Iy_wind(:)]/numel(Ix_wind);
        E(i,j) = min(eig(C));
    end
end
hitMap = E>tau;

flowField = zeros([iSzSml 2])*nan;
for i = 1:iSzSml(1)
    for j = 1:iSzSml(2)
        x = max(i-floor(wSzSml/2),1):min(i-floor(wSzSml/2)+wSzSml-1,iSzSml(1));
        y = max(j-floor(wSzSml/2),1):min(j-floor(wSzSml/2)+wSzSml-1,iSzSml(2));
        
        Ix_wind = IxSml(x,y);
        Iy_wind = IySml(x,y);
        It_wind = ItSml(x,y);
        
        C = [Ix_wind(:)'*Ix_wind(:) Ix_wind(:)'*Iy_wind(:);
             Ix_wind(:)'*Iy_wind(:) Iy_wind(:)'*Iy_wind(:)]/numel(Ix_wind);
        b = -[Ix_wind(:)'*It_wind(:); Iy_wind(:)'*It_wind(:)]/numel(Ix_wind);
        
        if min(eig(C))<tau || numel(Ix_wind) < 8
            continue
        end
        
        flowField(i,j,:) = C\b;
    end
end

u = flowField(:,:,1);
v = flowField(:,:,2);

