function [ p1, valid_mask ] = LK_pyrd_optical_flow(img0, img1, p0, n_iter, win_size, n_level, min_eig)
% accepts gray scaled input
% points are in 2*N format, mask is N*1 format

% build pyramid first
[pyrd_im0, pyrd_im0_x, pyrd_im0_y] = build_pryd(img0, n_level);
[pyrd_im1] = build_pryd(img1, n_level);

% compute prymd optical flow
valid_mask = ones(1, size(p0,2));
p1 = p0 ./ 2^(n_level-1);

for level = n_level:-1:1
    [p1, valid_mask]=...
        compute_iterative_optical_flow(pyrd_im0{level}, pyrd_im1{level}, ...
        pyrd_im0_x{level},...
        pyrd_im0_y{level},...
        n_iter, win_size, min_eig, p0, p1, valid_mask, level);
    
    if level ~= 1
        p1 = p1 * 2;
    end
end

end

function [p1, valid_mask] = compute_iterative_optical_flow(im0, im1, im0_x, im0_y, n_iter, win_size, min_eig, p0, p1, valid_mask, level)
p0 = p0 .* (1/2^(level-1));
ip0 = ceil(p0);

for iter = 1:n_iter
    ip1 = ceil(p1);

    for i=1:size(p0,2)
        if valid_mask(i)
            % collect spatial gradient around subsampled points, ip0
            % and temporal gradient around initial points p1
            x_0 = ip0(1,i);
            y_0 = ip0(2,i);
            x_1 = ip1(1,i);
            y_1 = ip1(2,i);
            
            p_halfx = min([floor(win_size/2), size(im0,2)-x_0, size(im1,2)-x_1]);
            p_halfy = min([floor(win_size/2), size(im0,1)-y_0, size(im1,1)-y_1]);
            
            n_halfx = min([ceil(win_size/2), x_0, x_1]);
            n_halfy = min([ceil(win_size/2), y_0, y_1]);
            
            l_0 = x_0 - n_halfx + 1;
            r_0 = x_0 + p_halfx;
            t_0 = y_0 - n_halfy + 1;
            b_0 = y_0 + p_halfy;
            
            l_1 = x_1 - n_halfx + 1;
            r_1 = x_1 + p_halfx;
            t_1 = y_1 - n_halfy + 1;
            b_1 = y_1 + p_halfy;
            
            d_x = im0_x(t_0:b_0, l_0:r_0);
            d_y = im0_y(t_0:b_0, l_0:r_0);
            I_xy = [d_x(:) d_y(:)]';
            
            d_t = reshape(im1(t_1:b_1, l_1:r_1)-im0(t_0:b_0, l_0:r_0), 1,[]);
            
            C = I_xy * I_xy' / size(I_xy, 2);
            b = - I_xy * d_t' / size(I_xy, 2);
            

            if min(eig(C)) < min_eig
                valid_mask(i) = 0;
            else
                p1(:,i) = p1(:,i) + C\b;
                if (p1(1,i)<=0 || p1(1,i)>size(im1,2) || p1(2,i)<=0 || p1(2,i)>size(im1,1))
                    valid_mask(i) = 0;
                end
            end
        end
    end
end

end

function [pyrd_im, varargout] = build_pryd(img, n_level)
pyrd_im = cell(n_level, 1);
pyrd_im{1} = img;

for level=2:n_level
    pyrd_im{level} = impyramid(pyrd_im{level-1}, 'reduce');
end

if nargout > 1
    hx = [-1,0,1];
    varargout{1} = cell(n_level, 1);
    varargout{2} = cell(n_level, 1);
    for level = 1:n_level
        varargout{1}{level} = imfilter(pyrd_im{level}, hx, 'replicate');
        varargout{2}{level} = imfilter(pyrd_im{level}, hx', 'replicate');
    end
    
end

end